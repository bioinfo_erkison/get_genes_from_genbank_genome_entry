#!/usr/bin/python3

from Bio import SeqIO, SeqFeature
from Bio.SeqRecord import SeqRecord
import os, argparse, sys

def parse_arguments():
    description = """
    A script to retrieve selected genes based on known locus tags from a genbank genome

    """
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter,)
    parser.add_argument('-f', '--genbank_filepath', help='path to the genbank genome file',required=True)
    parser.add_argument('-l', '--locus_tag_list_path', help='path to a plain text file containing locus tags (one on each line)',required=True)
    parser.add_argument('-c', '--gene_length_cut_off', help='sequence length cutoff for allowed CDSs', type=int, default=100)
    options = parser.parse_args()
    return options

def parse_locus_tags(locus_tag_list_path):
    with open(locus_tag_list_path, "r") as f:
        locus_tags = []
        for locus_tag in f:
            locus_tag = locus_tag.strip()
            if locus_tag == "": pass
            else:
                locus_tags.append(locus_tag)
    return locus_tags

def parse_genbank_record_for_locus_tag_positions(genbank_filepath, locus_tags, gene_length_cut_off):
    seq_record = next(SeqIO.parse(open(genbank_filepath), "genbank"))
    cds_list = []
    # Loop over the genome file, get the CDS features on each of the strands, only retrieve features matching any of the given locus tags

    for feature in seq_record.features:
        if feature.type == "CDS":
            try:
                for tag in locus_tags:
                    if tag in feature.qualifiers['locus_tag']: 
                        print("Found ", tag)
                        tag_start = feature.location._start.position
                        tag_end = feature.location._end.position
                        try: gene_name = feature.qualifiers["gene"][0]
                        except: gene_name = "unknown"
                        if feature.strand == -1:
                            strand = "-"
                        elif feature.strand == 1:
                            strand = "+"
                        else:
                            sys.stderr.write(
                                "No strand indicated for tag %s (%d-%d). Assuming + strand\n" % (tag, tag_start, tag_end)
                            )
                            strand = "+"
                        cds_list.append((tag_start, tag_end, strand, gene_name, tag))
                        break
                    else:
                        continue
            except:
                    continue
    return cds_list, seq_record

def extract_sequences(cds_list, seq_record, gene_length_cut_off):
    gene_records = []
    for cds in cds_list:
        gene_start = cds[0]
        gene_end = cds[1]
        strand = cds[2]
        gene_name = cds[3]
        tag = cds[4]
        gene_seq = seq_record.seq[gene_start:gene_end]
        if len(gene_seq) >= gene_length_cut_off:
            gene_records.append(
                SeqRecord(
                    gene_seq,
                    id=gene_name,
                    description="%s %s %d-%d %s"
                    % (seq_record.name, tag,  gene_start + 1, gene_end, strand),
                )
            )
    return gene_records

def write_output(genbank_filepath, gene_records):
    outpath = os.path.splitext(os.path.basename(genbank_filepath))[0] + "_select_genes.fasta"
    SeqIO.write(gene_records, open(outpath, "w"), "fasta")
    print ("\nSuccess! Output written to %s" % outpath)

def main():
    options = parse_arguments()

    genbank_filepath = options.genbank_filepath
    locus_tag_list_path = options.locus_tag_list_path
    gene_length_cut_off = options.gene_length_cut_off
    
    locus_tags = parse_locus_tags(locus_tag_list_path)
    cds_list, seq_record = parse_genbank_record_for_locus_tag_positions(genbank_filepath, locus_tags, gene_length_cut_off)
    gene_records = extract_sequences(cds_list, seq_record, gene_length_cut_off)
    write_output(genbank_filepath, gene_records)

if __name__ == "__main__":
    main()

