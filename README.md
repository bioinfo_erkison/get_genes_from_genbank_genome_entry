# Get genes from Genbank genome entry #
*Author: Erkison Odih - erkisonodih@gmail.com*
*Twitter: @bioinfo_erkison*

# Description #
A script to retrieve selected genes based on known locus tags from a downloaded genbank genome. 

# Dependencies #
Biopython
This is available as a PyPi package for Python3
```
pip3 install biopython
```
# Installation #
Simply download the "get_genes_from_genbank_genome_entry.py" script within a bin directory that is available in your path. 

```
cd some/directory/
git clone https://gitlab.com/bioinfo_erkison/get_genes_from_genbank_genome_entry.git
cd get_genes_from_genbank_genome_entry
cp get_genes_from_genbank_genome_entry.py /some/bin/directory/available/in/your/path
```

# Usage #
```
get_genes_from_genbank_genome_entry.py [-h] -f GENBANK_FILEPATH -l LOCUS_TAG_LIST_PATH [-c GENE_LENGTH_CUT_OFF]
```

